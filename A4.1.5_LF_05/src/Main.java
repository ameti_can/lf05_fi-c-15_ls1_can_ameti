import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {
	
	private static final DecimalFormat df = new DecimalFormat("0.0");
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie das Körpergwicht in kg ein: ");
		double koerpergewicht = tastatur.nextDouble();
		System.out.println("Geben Sie die Körpergröße in cm ein:");
		float koerpergroeße = tastatur.nextInt();
		System.out.println("Geben Sie ihr Geschlecht ein: ");
		char geschlecht = tastatur.next().charAt(0);
		
		String geschlechtAusgabe = null;
		
		if ( geschlecht == 'm' ) {
			geschlechtAusgabe = "männlich";
		} else {
			geschlechtAusgabe = "weiblich";
		}
		
		System.out.println("Das angegebene Körpergewicht beträgt: " + koerpergewicht);
		System.out.println("Die angegebene Körpergröße beträgt: " + koerpergroeße);
		
		System.out.println("");
		System.out.println("Der BMI wird nach folgenden Regeln klassifiziert: ");
		System.out.println("");
		tabellenAusgabe();
		System.out.println("");
		bmiAusgabe(koerpergewicht, koerpergroeße, geschlecht, geschlechtAusgabe);
		
	}
	
	public static void bmiAusgabe(double koerpergewicht, float koerpergroeße, char geschlecht, String geschlechtAusgabe) {
		
		if ( bmiBerechnen(koerpergewicht, koerpergroeße) < 20 && geschlecht == 'm' ) {
			
			System.out.println("Der errechnete Body Mass Index beträgt: " + df.format(bmiBerechnen(koerpergewicht, koerpergroeße)) + ". Da Sie als Geschlecht " + geschlechtAusgabe + " angegeben haben sind sie laut den obengenannten Klassifikationen untergewichtig.");
		
		} else if ( bmiBerechnen(koerpergewicht, koerpergroeße) >= 20 && bmiBerechnen(koerpergewicht, koerpergroeße) <= 25 && geschlecht == 'm' ) {
			
			System.out.println("Der errechnete Body Mass Index beträgt: " + df.format(bmiBerechnen(koerpergewicht, koerpergroeße)) + ". Da Sie als Geschlecht " + geschlechtAusgabe + " angegeben haben sind sie laut den obengenannten Klassifikationen normalgewichtig.");
		
		} else if ( bmiBerechnen(koerpergewicht, koerpergroeße) > 25 && geschlecht == 'm' ) {
			
			System.out.println("Der errechnete Body Mass Index beträgt: " + df.format(bmiBerechnen(koerpergewicht, koerpergroeße)) + ". Da Sie als Geschlecht " + geschlechtAusgabe + " angegeben haben sind sie laut den obengenannten Klassifikationen übergewichtig.");
		
		} else if ( bmiBerechnen(koerpergewicht, koerpergroeße) <19 && geschlecht == 'w' ) {
		
			System.out.println("Der errechnete Body Mass Index beträgt: " + df.format(bmiBerechnen(koerpergewicht, koerpergroeße)) + ". Da Sie als Geschlecht " + geschlechtAusgabe + " angegeben haben sind sie laut den obengenannten Klassifikationen untergewichtig.");
		
		} else if ( bmiBerechnen(koerpergewicht, koerpergroeße) >= 19 && bmiBerechnen(koerpergewicht, koerpergroeße) <= 24 && geschlecht == 'w' ) {
			
			System.out.println("Der errechnete Body Mass Index beträgt: " + df.format(bmiBerechnen(koerpergewicht, koerpergroeße)) + ". Da Sie als Geschlecht " + geschlechtAusgabe + " angegeben haben sind sie laut den obengenannten Klassifikationen normalgewichtig.");
		
		} else if ( bmiBerechnen(koerpergewicht, koerpergroeße) > 24 && geschlecht == 'w' ) {
			
			System.out.println("Der errechnete Body Mass Index beträgt: " + df.format(bmiBerechnen(koerpergewicht, koerpergroeße)) + ". Da Sie als Geschlecht " + geschlechtAusgabe + " angegeben haben sind sie laut den obengenannten Klassifikationen übergewichtig.");
		
		}
	}
	
	public static void tabellenAusgabe() {
		String s1 = "Klassifikation";
		String s2 = "m";
		String s3 = "w";
		String s4 = "Untergewicht";
		String s5 = "Normalgewicht";
		String s6 = "Übergewicht";
		String s7 = "< 20";
		String s8 = "20 - 25";
		String s9 = "> 25";
		String s10 = "< 19";
		String s11 = "19 - 24";
		String s12 = "> 24";

		System.out.printf( "\n%-12s |     %s     |     %s     \n", s1, s2, s3);
		System.out.println("---------------------------------------");
		System.out.printf( "%s   |   %s    |   %s\n", s4, s7, s10);
		System.out.printf( "%s  |  %s  |  %s\n", s5, s8, s11);
		System.out.printf( "%s    |   %s    |   %s\n", s6, s9, s12);
	}
	
	public static double bmiBerechnen(double koerpergewicht, float koerpergroeße) {
		return koerpergewicht / Math.pow(koerpergroeße / 100, 2);
	}
}
