﻿import java.util.Scanner;

class Fahrkartenautomat { 
	
	static Scanner tastatur = new Scanner(System.in);
	
    static double zuZahlenderBetrag; 
    static double eingezahlterGesamtbetrag;
    static double eingeworfeneMünze;
    static double rückgabebetrag;
    static short  ticketsAnzahl;

	
    public static void main(String[] args) {
    	
       System.out.print("Ticketpreis (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.print("Ticketanzahl: ");
       ticketsAnzahl = tastatur.nextShort();
       
       geldeinwurf(eingezahlterGesamtbetrag, ticketsAnzahl, zuZahlenderBetrag);
       
       fahrscheinAusgabe();
       
       rückgeldberechnung();
       
       
       
       
    }
       
       // Geldeinwurf
       // -----------
      public static void geldeinwurf(double eingezahlterGesamtbetrag, short ticketsAnzahl, double zuZahlenderBetrag) {
	       eingezahlterGesamtbetrag = 0.0;
	       zuZahlenderBetrag = ticketsAnzahl * zuZahlenderBetrag;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.print("Noch zu zahlen: ");
	    	   System.out.printf("%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
       }

       // Fahrscheinausgabe
       // -----------------
      public static void fahrscheinAusgabe() {
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
       
       }
       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
      public static void rückgeldberechnung() {
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.print("Der Rückgabebetrag in Höhe von ");
	    	   System.out.printf("%.2f", rückgabebetrag);
	    	   System.out.print(" EURO");
	    	   System.out.println(" wird in folgenden Münzen ausgezahlt:");
	
	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.");
	    }
}
// Für den Datentyp Short habe ich mich entschieden für den äußerst seltenen Fall, dass jemand mehr als 128 Tickets kaufen möchte, also die Obergrenze von Byte.
// Bei der Berechnung ticketsAnzahl * zuZahlenderBetrag wird der Wert, welcher beim Ticketpreis angegeben wird, mit dem Wert multipliziert, welcher bei der Ticket Anzahl angegeben wird.