import java.util.Scanner;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Main {
	
	 private static final DecimalFormat df = new DecimalFormat("0.00");
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie den Bestellwert ein: ");
		double bestellwert = tastatur.nextDouble();
		System.out.println("Nachdem der Bestellwert durch das Rabbatssystem gelaufen ist und die MwSt. angerechnet wurde entpspricht der Bestellwert " + df.format(ermaeßigterBestelltwert(bestellwert)) + "€.");
	}
	public static double ermaeßigterBestelltwert( double bestellwert) {
		if ( bestellwert < 100 ) {
			return (bestellwert * 1.19) - (bestellwert * 1.19) * 0.10;
		} else if ( bestellwert < 500 ) {
			return (bestellwert * 1.19) - (bestellwert * 1.19) * 0.15;
		} else {
			return (bestellwert * 1.19) - (bestellwert * 1.19) * 0.20;
		}
	}
}
