import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl f�r den Wert X ein:");
		int x = scan.nextInt();
		System.out.println("Geben Sie eine Zahl f�r den Wert Y ein:");
		int y = scan.nextInt();
		System.out.println("Geben Sie eine Zahl f�r den Wert Z ein:");
		int z = scan.nextInt();
		
		Aufgabe1(x, y, z);
		// Wenn die 1.Zahl gr��er als die 2.Zahl und die 3. Zahl ist, soll eine Meldung
		// ausgegeben werden (If mit && / Und)
		
		Aufgabe2(x, y, z);
		// Wenn die 3.Zahl gr��er als die 2.Zahl oder die 1. Zahl ist, soll eine Meldung
		// ausgegeben werden (If mit || / Oder)
		
		Aufgabe3(x, y, z);
		// Geben Sie die gr��te der 3 Zahlen aus. (If-Else mit && / Und)

	}	
	
	
	public static void Aufgabe1(int x, int y, int z) {
		if (x > y && x > z) {
			System.out.println("Der Wert X (" + x + ") ist gr��er als die Werte Y (" + y + ") und Z (" + z + ").");
		} 
	}
	
	public static void Aufgabe2(int x, int y, int z) {
		if (z > x || z > y) {
			System.out.println("Der Wert Z (" + z + ") ist gr��er als der Werte X (" + x + ") oder Y (" + y + ").");
		}
	}

	public static void Aufgabe3(int x, int y, int z) {
		if (x > y && x > z) {
			System.out.println("Der Wert X (" + x + ") ist die gr��te Zahl.");
		} else if (y > x && y > z) {
			System.out.println("Der Wert Y (" + y + ") ist die gr��te Zahl.");
		} else if ( z > y && z > x) {
			System.out.println("Der Wert Z (" + z + ") ist die gr��te Zahl.");
		} else {
			System.out.println("Es konnte keine gr��te Zahl ermittelt werden.");
		}
	}
}
