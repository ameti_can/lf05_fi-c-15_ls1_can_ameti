import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Jahreszahl an, welche gepr�ft werden soll. Im folgenden wird berechnet ob dies ein Schaltjahr war, ist oder wird. ");
		int jahresZahl = tastatur.nextInt();
		
		if ( regel1(jahresZahl) == true || regel2und3(jahresZahl) == true ) {
			System.out.println("Das Jahr " + jahresZahl + " war, ist oder wird ein Schaltjahr.");
		} else {
			System.out.println("Das Jahr " + jahresZahl + " war, ist oder wird kein Schaltjahr.");
		}
		
	}
	
	public static boolean regel1(int jahresZahl) {
		if ( ( jahresZahl % 4 ) == 0 ) {
			if ( ( jahresZahl % 100 ) == 0 ) {
				if ( ( jahresZahl % 400 ) == 0 ) {
					return true;
				} else {
					return false;
				
			}
	
		} else if ( ( jahresZahl % 4 ) == 0 ) {
					return true;
				} else {
					return false;
			} 
		} else if ( ( jahresZahl % 100 ) == 0 ) {
			if ( ( jahresZahl % 400 ) == 0 ) {
					return true;
			}
		} return false;
	} 
	
	public static boolean regel2und3(int jahresZahl) {
		if ( ( jahresZahl % 100 ) == 0 ) {

				if ( ( jahresZahl % 400 ) == 0 ) {
					return true;
				} else {
					return false;
				
			}
	
		} 
		return false;
	}
}
