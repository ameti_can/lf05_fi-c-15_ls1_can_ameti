import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie den Nettowert ein welcher versteuert werden soll:");
		float nettowert = scan.nextFloat();
		
		System.out.println("Geben Sie nun den gewünschten Steuersatz an. Dies können sie mit den Buchstaben n und j machen.");
		System.out.println("n = voller Steuersatz (19%)");
		System.out.println("j = ermäßigter Steuersatz (7%)");
		char steuersatz = scan.next().toCharArray()[0];
		
		if ( steuersatz == 'n' ) {
			System.out.println("Sie haben den vollen Steuersatz gewählt. Somit entspricht der Bruttobetrag " + bruttobetragVollerSteuersatz(nettowert) + "€.");
		} else if ( steuersatz == 'j' ) {
			System.out.println("Sie haben den ermäßigten Steuersatz gewählt. Somit entspricht der Bruttobetrag " + bruttobetragErmaeßigterSteuersatz(nettowert) + "€.");
		}
		
		
		

	}
	
	public static float bruttobetragVollerSteuersatz(float nettowert) {
		
		float bruttobetragVollerSteuersatz;
		
		bruttobetragVollerSteuersatz = (nettowert * 0.19f) + nettowert;

		return bruttobetragVollerSteuersatz;
	}
	
	public static float bruttobetragErmaeßigterSteuersatz(float nettowert) {
		
		float bruttobetragErmaeßigterSteuersatz;
		
		bruttobetragErmaeßigterSteuersatz = (nettowert * 0.07f) + nettowert;

		return bruttobetragErmaeßigterSteuersatz;
	}

}
