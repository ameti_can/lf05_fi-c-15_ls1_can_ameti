import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		int mausAnzahl = 0;
		double einzelpreis = 0.00;
		
		System.out.println("Geben Sie die Bestellmenge der M�use ein:");
		mausAnzahl = tastatur.nextInt();
		System.out.println("Geben Sie den St�ckpreis der Maus ein (ohne Mehrwertsteuer):");
		einzelpreis = tastatur.nextFloat();
		
		if ( mausAnzahl < 10 ) {
			System.out.println("Der Rechnungsbetrag entspricht " + (mausAnzahl * (einzelpreis * 1.19) + 10) + "�. Dies ergibt sich aus der Mausanzahl von " + mausAnzahl + " zum St�ckpreis von " + einzelpreis + "� und der Mehrwertsteuer von 19%. Zuz�glich dessen wird eine Lieferpauschale von 10� angerechnet da die Bestellmenge kleiner als 10 ist.");
		} else {
			System.out.println("Der Rechnungsbetrag entspricht " + (mausAnzahl * (einzelpreis * 1.19) + 10) + "�. Dies ergibt sich aus der Mausanzahl von " + mausAnzahl + " zum St�ckpreis von " + einzelpreis + "� und der Mehrwertsteuer von 19%. Da die Bestellmenge der Mausanzahl �betroffen wurde, werden keine zus�tzlichen Lieferkosten anfallen.");
		}
	}
}