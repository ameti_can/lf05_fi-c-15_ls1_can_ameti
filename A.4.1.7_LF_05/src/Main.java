import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie drei Zahlen ein die logisch sortiert werden sollen: ");
		int zahl1 = tastatur.nextInt();
		int zahl2 = tastatur.nextInt();
		int zahl3 = tastatur.nextInt();
	
		zahlenSortieren(zahl1, zahl2, zahl3);
		
	}
	
	public static void zahlenSortieren(int zahl1, int zahl2, int zahl3) {
		
		if (zahl1 < zahl2 && zahl1 < zahl3 && zahl2 < zahl3) {
			System.out.println("1. " + zahl1);
			System.out.println("2. " + zahl2);
			System.out.println("3. " + zahl3);
		} else if (zahl1 < zahl2 && zahl1 < zahl3 && zahl3 < zahl2) {
			System.out.println("1. " + zahl1);
			System.out.println("2. " + zahl3);
			System.out.println("3. " + zahl2);			
		} else if (zahl2 < zahl1 && zahl2 < zahl3 && zahl1 < zahl3) {
			System.out.println("1. " + zahl2);
			System.out.println("2. " + zahl1);
			System.out.println("3. " + zahl3);
		} else if (zahl2 < zahl1 && zahl2 < zahl3 && zahl3 < zahl1) {
			System.out.println("1. " + zahl2);
			System.out.println("2. " + zahl3);
			System.out.println("3. " + zahl1);
		} else if (zahl3 < zahl1 && zahl3 < zahl1 && zahl1 < zahl2) {
			System.out.println("1. " + zahl3);
			System.out.println("2. " + zahl1);
			System.out.println("3. " + zahl2);
		} else if (zahl3 < zahl1 && zahl3 < zahl1 && zahl2 < zahl1) {
			System.out.println("1. " + zahl3);
			System.out.println("2. " + zahl2);
			System.out.println("3. " + zahl1);
		}
	}
}
